<?php

class StudentInfo
{
    public $myMessage=" ";

    static function message($msg){

        echo "$msg <br>";
    }

    function __construct()
    {
        echo "Thanks For Creating me ";
    }

    function __destruct()
    {
        echo "Good bye ";
    }

}
$rahim = new StudentInfo();

unset($rahim);

StudentInfo::message("Data Has been inserted!");